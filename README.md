# 小皮博客后端

#### 介绍
* 皮皮豪的第一个java项目
* 测试表示没有问题了，基于MIT协议，随便了，反正 我不在意协议
* 前台用的thymeleaf渲染的用了jquery+vue+bootstrap
* 说实话，我也不喜欢css,所以我自己没有写一个css文件，全用的bootstrap
* 后台，是我自己乱想的一个规范，搞了一个jquery+vue的项目
* 后台，是真的前后端分离了，但是前台考虑到SEO，所以用的是thymeleaf，这个是为了方便百度收录
* 后台做了加密，不过有两点我没做，就是图片上传和验证码 
* 我觉得个这项目是一个毛板，专用于项目的快速搭建，我以后项目都会基于这个项目进行开发
* 对于我来说相当于一个脚手架
* 毕竟我这个也算功能齐全了
* 后台源码 的链接  https://gitee.com/pphboy/xpblogmanager


#### 软件架构
**用的jpa，没有一行sql,也没有sql代码 ,我这个人懒，不喜欢写sql，直接运行就可以了**
* springboot 
* springmvc 
* springdatajpa
* mysql
* thymeleaf



#### 安装教程

1. 用的jpa，没有一行sql,也没有sql代码 ，直接运行就可以了
2. application.yml配置一个mysql，就能用了，没什么教程

#### 使用说明

1.  跑起来就能用

#### 参与贡献

1.  Fork 本仓库
2.  皮皮豪全程开发
3.  皮皮豪博客 https://www.pipihao.com
4.  小二社区 https://bbs.xiao2.me
