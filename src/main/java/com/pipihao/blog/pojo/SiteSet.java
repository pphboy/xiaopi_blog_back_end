package com.pipihao.blog.pojo;

import org.hibernate.annotations.Proxy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity  //代表此类为一个表的映射entity类
@Table(name="xp_siteset")  //设置对应的表名
@Proxy(lazy = false)
public class SiteSet {
    @Id
    @Column(name = "id")
    private Integer id;

    @Column(name = "webSiteName",nullable = false)
    private String webSiteName;

    @Column(name = "keywords",nullable = false)
    private String keywords;

    @Column(name = "webSiteIntroduce",nullable = false)
    private String webSiteIntroduce;

    @Column(name ="siteShow",nullable = false)
    private Boolean siteShow;

    @Column(name ="LeaveShow",nullable = false)
    private  Boolean leaveShow;

    public String getWebSiteName() {
        return webSiteName;
    }

    public void setWebSiteName(String webSiteName) {
        this.webSiteName = webSiteName;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getWebSiteIntroduce() {
        return webSiteIntroduce;
    }

    public void setWebSiteIntroduce(String webSiteIntroduce) {
        this.webSiteIntroduce = webSiteIntroduce;
    }

    public Boolean getSiteShow() {
        return siteShow;
    }

    public void setSiteShow(Boolean siteShow) {
        this.siteShow = siteShow;
    }

    public Boolean getLeaveShow() {
        return leaveShow;
    }

    public void setLeaveShow(Boolean leaveShow) {
        this.leaveShow = leaveShow;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "SiteSet{" +
                "webSiteName='" + webSiteName + '\'' +
                ", keywords='" + keywords + '\'' +
                ", webSiteIntroduce='" + webSiteIntroduce + '\'' +
                ", siteShow=" + siteShow +
                ", leaveShow=" + leaveShow +
                '}';
    }
}
