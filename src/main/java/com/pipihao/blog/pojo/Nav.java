package com.pipihao.blog.pojo;

import org.hibernate.annotations.Proxy;

import javax.persistence.*;

@Entity  //代表此类为一个表的映射entity类
@Table(name="xp_nav")  //设置对应的表名
@Proxy(lazy = false)
public class Nav {

    @Id  //此备注代表该字段为该类的主键
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    private String navName;

    private String navUrl;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNavName() {
        return navName;
    }

    public void setNavName(String navName) {
        this.navName = navName;
    }

    public String getNavUrl() {
        return navUrl;
    }

    public void setNavUrl(String navUrl) {
        this.navUrl = navUrl;
    }
}
