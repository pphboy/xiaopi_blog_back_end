package com.pipihao.blog.pojo;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.pipihao.blog.util.HtmlUtil;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.Proxy;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.persistence.GenerationType;
import java.io.Serializable;
import java.util.*;

/**
 * 文章类
 */
@Entity  //代表此类为一个表的映射entity类
@Table(name="xp_article")  //设置对应的表名
@Proxy(lazy = false)
public class Article implements Serializable {
    /** 主键-id uuid */
    @Id  //此备注代表该字段为该类的主键
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    /** 数字,具有唯一性 */
    //nullable - 是否可以为null,默认为true   unique - 是否唯一,默认为false
    @Column(name="title",nullable=false,unique=false,length = 50)
    private String title;


    // 用户名
    @Column(name="username",unique=false,nullable=false,length = 20)
    private String username;

    // 文章内容
    @Column(name="content",unique=false,nullable=false,length = 9999)
    private String content;

    //
    @Column(name="indexImage")
    private String indexImage;

    @Column(name="watchNum")
    private Long watchNum;

    //为0则是草稿，为1则是发表
    @Column(name="showOk",unique=false,nullable=false)
    private Boolean showOk;

    /** 部门创建时间 */
    @Column(name="createTime")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /** 部门修改时间 */
    @Column(name="updateTime")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date editTime;


    @ManyToMany(targetEntity =Classes.class,cascade = CascadeType.ALL)
    @JoinTable(name = "article_class",
            //joinColumns,当前对象在中间表中的外键
            joinColumns = {@JoinColumn(name = "article_id",referencedColumnName = "id")},
            //inverseJoinColumns,对方对象在中间表中的外键
            inverseJoinColumns = {@JoinColumn(name = "classes_id",referencedColumnName = "id")}
    )
    private List<Classes> classesList = new ArrayList<>();

    @ManyToMany(targetEntity = Label.class,cascade = CascadeType.ALL)
    @JoinTable(name = "article_label",
            //joinColumns,当前对象在中间表中的外键
            joinColumns = {@JoinColumn(name = "article_id",referencedColumnName = "id")},
            //inverseJoinColumns,对方对象在中间表中的外键
            inverseJoinColumns = {@JoinColumn(name = "label_id",referencedColumnName = "id")}
    )
    private List<Label> labelList= new ArrayList<>();

    @OneToMany(mappedBy = "article",cascade = CascadeType.REMOVE,fetch = FetchType.EAGER)
    private List<LeavingMessage>  leavingMessageList= new ArrayList<>();

    /**
     *
     * @return
     */
    public String getNoHtmlTextArticle(){
        return HtmlUtil.getText(this.content);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getIndexImage() {
        return indexImage;
    }

    public void setIndexImage(String indexImage) {
        this.indexImage = indexImage;
    }

    public Long getWatchNum() {
        return watchNum;
    }

    public void setWatchNum(Long watchNum) {
        this.watchNum = watchNum;
    }

    public Boolean getShowOk() {
        return showOk;
    }

    public void setShowOk(Boolean showOk) {
        this.showOk = showOk;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getEditTime() {
        return editTime;
    }

    public void setEditTime(Date editTime) {
        this.editTime = editTime;
    }

    public List<Classes> getClassesList() {
        return classesList;
    }

    public void setClassesList(List<Classes> classesList) {
        this.classesList = classesList;
    }



    public List<Label> getLabelList() {
        return labelList;
    }

    public void setLabelList(List<Label> labelList) {
        this.labelList = labelList;
    }

    public List<LeavingMessage> getLeavingMessageList() {
        return leavingMessageList;
    }

    public void setLeavingMessageList(List<LeavingMessage> leavingMessageList) {
        this.leavingMessageList = leavingMessageList;
    }

}
