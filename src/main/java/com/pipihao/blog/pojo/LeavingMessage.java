package com.pipihao.blog.pojo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.pipihao.blog.util.HtmlUtil;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * 留言类
 */
@Entity  //代表此类为一个表的映射entity类
@Table(name="xp_leavemsg")  //设置对应的表名
public class LeavingMessage implements Serializable {

    /** 主键-id uuid */
    @Id  //此备注代表该字段为该类的主键
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    /** 数字,具有唯一性 */
    //nullable - 是否可以为null,默认为true   unique - 是否唯一,默认为false
    @Column(name="email",nullable=false,length = 20)
    private String email;


    /** 数字,具有唯一性 */
    //nullable - 是否可以为null,默认为true   unique - 是否唯一,默认为false
    @Column(name="visitorName",nullable=false,length = 20)
    private String visitorName;

    //评论ip
    @Column(name="visitorIp",nullable=false)
    private String visitorIp;
    //评论时间
    @Column(name="createTime",nullable=false)
    private  Date createTime;

    /** 数字,具有唯一性 */
    //nullable - 是否可以为null,默认为true   unique - 是否唯一,默认为false
    @Column(name="content",nullable=false,length = 200)
    private String content;

    /**
     * 配置联系人到客户多对一关系
     * 使用注解形式配置多对一关系
     * 1.配置表关系   @ManyToOne:配置多对一关系  targetEntity:对方的实体类字节码
     * 2.配置外键(中间表)
     * 配置外键的过程，配置到了多的一方，就会在多的一方维户外键
     */
    @JsonIgnore
    @ManyToOne(targetEntity = Article.class,fetch = FetchType.EAGER)
    @JoinColumn(name = "article_id",referencedColumnName = "id")
    private Article article;

    public String getContentHtml(){
        return HtmlUtil.getText(this.content);
    }
    public String getVisitorNameHtml(){
        return HtmlUtil.getText(this.visitorName);
    }

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getVisitorName() {
        return visitorName;
    }

    public void setVisitorName(String visitorName) {
        this.visitorName = visitorName;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getVisitorIp() {
        return visitorIp;
    }

    public void setVisitorIp(String visitorIp) {
        this.visitorIp = visitorIp;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "LeavingMessage{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", visitorName='" + visitorName + '\'' +
                ", visitorIp='" + visitorIp + '\'' +
                ", createTime=" + createTime +
                ", content='" + content + '\'' +
//                ", article=" + article +
                '}';
    }
}
