package com.pipihao.blog.pojo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Proxy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * 标签类
 */
@Entity  //代表此类为一个表的映射entity类
@Table(name="xp_lable")  //设置对应的表名
@Proxy(lazy = false)
public class Label implements Serializable {

    /** 主键-id uuid */
    @Id  //此备注代表该字段为该类的主键
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    //介绍
    @Column(name="labelName",nullable=false,unique = false,length = 20)
    private String labelName;

    // 多对多基本不用级联删除
    @ManyToMany(mappedBy = "labelList")
    @JsonIgnore
    private List<Article> articleList= new ArrayList<>();


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLabelName() {
        return labelName;
    }

    public void setLabelName(String labelName) {
        this.labelName = labelName;
    }

    public List<Article> getArticleList() {
        return articleList;
    }

    public void setArticleList(List<Article> articleList) {
        this.articleList = articleList;
    }

    @Override
    public String toString() {
        return "Label{" +
                "id=" + id +
                ", labelName='" + labelName + '\'' +
//                ", articleSet=" + articleSet +
                '}';
    }
}
