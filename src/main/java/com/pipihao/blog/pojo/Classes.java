package com.pipihao.blog.pojo;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * 分类类
 */
@Entity  //代表此类为一个表的映射entity类
@Table(name="xp_classes")  //设置对应的表名
public class Classes implements Serializable {

    /** 主键-id uuid */
    @Id  //此备注代表该字段为该类的主键
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    /** 数字,具有唯一性 */
    //分类名
    //nullable - 是否可以为null,默认为true   unique - 是否唯一,默认为false
    @Column(name="className",nullable=false,unique=true,length = 20)
    private String className;

    //介绍
    @Column(name="introduce",nullable=false,unique=true,length = 60)
    private String introduce;


    @JsonIgnore
    @ManyToMany(mappedBy = "classesList")
    private List<Article> articleSet= new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getIntroduce() {
        return introduce;
    }

    public void setIntroduce(String introduce) {
        this.introduce = introduce;
    }

    public List<Article> getArticleSet() {
        return articleSet;
    }

    public void setArticleSet(List<Article> articleSet) {
        this.articleSet = articleSet;
    }

    @Override
    public String toString() {
        return "Classes{" +
                "id=" + id +
                ", className='" + className + '\'' +
                ", introduce='" + introduce + '\'' +
//                ", articleSet=" + articleSet +
                '}';
    }
}
