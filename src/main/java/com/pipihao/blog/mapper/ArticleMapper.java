package com.pipihao.blog.mapper;

import com.pipihao.blog.pojo.Article;
import com.pipihao.blog.pojo.Classes;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;



@Repository
public interface ArticleMapper extends JpaRepository<Article,Long>,JpaSpecificationExecutor<Article>{

    //分页查询 用ClassID
    @Query(value="SELECT * from `xp_article` where `id` in (SELECT `article_id` from `article_class` WHERE `classes_id` = ?1 )",nativeQuery = true)
    Page<Article> findAllByClassId(Integer classId, Pageable pageable);

    //分页查询 用LableID
    @Query(value="SELECT * from `xp_article` where `id` in (SELECT `article_id` from `article_label` WHERE `label_id` = ?1 )",nativeQuery = true)
    Page<Article> findAllByLabelId(Integer labelId, Pageable pageable);

    //通过labelid查询当前label下的所有的article条数
    @Query(value="SELECT count(id) from `xp_article` where `id` in (SELECT `article_id` from `article_label` WHERE `label_id` = ?1 )",nativeQuery = true)
    Integer getArtileTotalNumberByLabelId(Integer labelId);

    //通过classId查询当前class下的所有的article条数
    @Query(value="SELECT count(id) from `xp_article` where `id` in (SELECT `article_id` from `article_class` WHERE `classes_id` = ?1 )",nativeQuery = true)
    Integer getArtileTotalNumberByClassId(Integer classId);
}
