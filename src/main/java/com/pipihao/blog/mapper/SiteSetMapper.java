package com.pipihao.blog.mapper;


import com.pipihao.blog.pojo.SiteSet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface SiteSetMapper extends JpaRepository<SiteSet,Integer>, JpaSpecificationExecutor<SiteSet> {
}
