package com.pipihao.blog.mapper;

import com.pipihao.blog.pojo.Nav;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface NavMapper extends JpaRepository<Nav,Integer>, JpaSpecificationExecutor<Nav> {
}
