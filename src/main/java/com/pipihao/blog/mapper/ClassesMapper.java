package com.pipihao.blog.mapper;

import com.pipihao.blog.pojo.Article;
import com.pipihao.blog.pojo.Classes;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClassesMapper extends JpaRepository<Classes,Integer>, JpaSpecificationExecutor<Classes> {

}
