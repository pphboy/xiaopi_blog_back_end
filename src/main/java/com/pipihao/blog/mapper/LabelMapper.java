package com.pipihao.blog.mapper;

import com.pipihao.blog.pojo.Label;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface LabelMapper extends JpaRepository<Label,Integer>, JpaSpecificationExecutor<Label> {
}
