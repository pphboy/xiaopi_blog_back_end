package com.pipihao.blog.mapper;

import com.pipihao.blog.pojo.Article;
import com.pipihao.blog.pojo.LeavingMessage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.logging.Level;

@Repository
public interface LeavingMessageMapper extends JpaRepository<LeavingMessage,Integer>, JpaSpecificationExecutor<LeavingMessageMapper> {
}
