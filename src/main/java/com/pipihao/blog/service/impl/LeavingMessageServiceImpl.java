package com.pipihao.blog.service.impl;

import com.pipihao.blog.mapper.LeavingMessageMapper;
import com.pipihao.blog.pojo.LeavingMessage;
import com.pipihao.blog.service.LeavingMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class LeavingMessageServiceImpl implements LeavingMessageService {

    @Autowired
    private LeavingMessageMapper leavingMessageMapper;

    /**
     * 保存一个leavingMessage
     * @param leavingMessage
     */
    @Override
    public void saveLeavingMessage(LeavingMessage leavingMessage) {
        leavingMessage.setCreateTime(new Date());
        this.leavingMessageMapper.save(leavingMessage);
    }

    /**
     * 分页查询 leavingMessage
     * @param rows
     * @param size
     */
    @Override
    public Page<LeavingMessage> findAllLeavingMessageByPage(Integer rows, Integer size) {
        Sort sort = Sort.by(Sort.Direction.DESC,"id");
        return this.leavingMessageMapper.findAll(PageRequest.of(rows,size,sort));
    }

    /**
     * 删除一个留言
     * @param id
     */
    @Override
    public void deleteLeavingMessageById(Integer id) {
        this.leavingMessageMapper.deleteById(id);
    }
}
