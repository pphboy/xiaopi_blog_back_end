package com.pipihao.blog.service.impl;

import com.pipihao.blog.mapper.*;
import com.pipihao.blog.pojo.Article;
import com.pipihao.blog.pojo.Classes;
import com.pipihao.blog.pojo.Label;
import com.pipihao.blog.service.ArticleService;
import com.pipihao.blog.service.SiteSetService;
import com.pipihao.blog.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Service
public class ArticleServiceImpl implements ArticleService {
    @Autowired
    private ArticleMapper articleMapper;
    @Autowired
    private ClassesMapper classesMapper;
    @Autowired
    private LabelMapper labelMapper;
    @Autowired
    private LeavingMessageMapper leavingMessageMapper;
    @Autowired
    private UserService userService;

    /**
     * 保存一个Article
     * @param article
     * @param class_ids
     */
    @Override
    @Transactional
    public void saveArticle(Article article,List<Integer> class_ids,List<Integer> labelIds){

        List<Classes> classesList= classesMapper.findAllById(class_ids);
        if (!labelIds.isEmpty()){
            article.getLabelList().addAll(this.labelMapper.findAllById(labelIds));
        }
        article.getClassesList().addAll(classesList);
        article.setUsername(userService.findUser().getUsername());
        article.setWatchNum(1l);
        //如果传来了id说明需要更新
        //且设置的时候也是为Null的
        if (article.getId() != null){
            //如果字符串的内容不为空，就执行这段代码
            Article articleById = this.findArticleById(article.getId());
            //不设置Id，就会新增，而不是更新
            article.setId(article.getId());
            article.setCreateTime(articleById.getCreateTime());
            article.getLeavingMessageList().addAll(articleById.getLeavingMessageList());
            article.setEditTime(new Date());
        }else{
            //如果没有id，则说明是创建一个文章
            article.setCreateTime(new Date());
            article.setEditTime(new Date());
        }
        this.articleMapper.save(article);
    }

    /**
     * 过通id查询一篇文章
     * @param id
     * @return
     */
    @Override
    public Article findArticleById(Long id){
        return articleMapper.getOne(id);
    }

    @Override
    public Page<Article> findArticleByPage(Integer rows,Integer size) {
        Sort sort = Sort.by(Sort.Direction.DESC,"id");
        Pageable pageable = PageRequest.of(rows,size,sort);
        return  articleMapper.findAll(pageable);
   }

    @Override
    public Page<Article> findHotArticleByPage() {
        Sort sort = Sort.by(Sort.Direction.DESC,"watchNum");
        Pageable pageable = PageRequest.of(0,10,sort);
        return  articleMapper.findAll(pageable);
    }

    @Override
    public void addWatchNumber(Article article) {
        article.setWatchNum(article.getWatchNum()+1);
        this.articleMapper.save(article);
    }



    /**
     * 查询所有的文章
     * @return
     */
    /*
    public List<Article> findAllArticle(){
        return articleMapper.findAll();
    }
    */

    /**
     * 修改 article
     * @param article
     */
    @Transactional
    @Override
    public void updateArticle(Article article){
        article.setEditTime(new Date());
        articleMapper.save(article);
    }

    /**
     * 删除一个article
     * @param id
     */
    @Transactional
    @Override
    public void deleteArticleById(Long id){
        Article article= this.findArticleById(id);
        article.setClassesList(new ArrayList<>());
        article.setLabelList(new ArrayList<>());
        //删除文章下面的留言，其他的分类和标签无需删除
        leavingMessageMapper.deleteAll(article.getLeavingMessageList());
        articleMapper.delete(article);
    }

    /**
     * 删除一个article,还有删除本文章上的classId所属的类
     * @param articleId
     * @param classId
     */
    @Transactional
    @Override
    public void deleteArticleClassesIdById(Long articleId,Integer classId){
        Article article= this.findArticleById(articleId);
        //查询classId对应的类
        Classes classes= this.classesMapper.getOne(classId);
        //删除该id对应的分类
        article.getClassesList().remove(classes);
        articleMapper.save(article);
    }

    /**
     * 删除一个label,还有删除本文章上的labelId所属的类
     * @param articleId
     * @param labelId
     */
    @Transactional
    @Override
    public void deleteArticleLabelById(Long articleId,Integer labelId){
        Article article= this.findArticleById(articleId);
        //查询classId对应的类
        Label label = this.labelMapper.getOne(labelId);
        //删除该id对应的分类
        article.getLabelList().remove(label);
        articleMapper.save(article);
    }

    @Override
    @Transactional
    public List<Article> findArticleByClassesId(Integer classId,Integer page,Integer size){
        Classes clazz= this.classesMapper.getOne(classId);
        List<Article> list= clazz.getArticleSet();
        Collections.reverse(list);
        List<Article> articles= new ArrayList<>();
        for (int i = (page-1)*size; i < page*size; i++) {
            if(i < list.size()) {
                articles.add(list.get(i));
            }
        }
        return articles;
    }

    @Override
    public List<Article> findArticleByLabelId(Integer labelId, Integer page, Integer size) {
        Label label = this.labelMapper.getOne(labelId);
        List<Article> list=label.getArticleList();
        Collections.reverse(list);
        List<Article> articles= new ArrayList<>();
        for (int i = (page-1)*size; i < page*size; i++) {
            if(i < list.size()) {
                 articles.add(list.get(i));
            }
        }
        return articles;
    }

    @Override
    public Page<Article> findAllByClassId(Integer classId, Integer page, Integer size) {
        return this.articleMapper.findAllByClassId(classId,PageRequest.of(page,size));
    }

    @Override
    public Page<Article> findAllByLabelId(Integer labelId, Integer page, Integer size) {
        return this.articleMapper.findAllByLabelId(labelId,PageRequest.of(page,size));
    }

    @Override
    public Integer getArtileTotalNumberByLabelId(Integer labelId) {
        return this.articleMapper.getArtileTotalNumberByLabelId(labelId);
    }

    @Override
    public Integer getArtileTotalNumberByClassId(Integer classId) {
        return this.articleMapper.getArtileTotalNumberByClassId(classId);
    }

}
