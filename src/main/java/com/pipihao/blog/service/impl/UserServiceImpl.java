package com.pipihao.blog.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pipihao.blog.mapper.UserMapper;
import com.pipihao.blog.pojo.User;
import com.pipihao.blog.service.UserService;
import com.pipihao.blog.util.JWTUtils;
import com.pipihao.blog.util.MD5Utils;
import com.pipihao.blog.util.UUIDUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.crypto.SecretKey;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    /**
     * 更新用户信息
     * @param user
     */
    @Override
    @Transactional
    public String updateUser(User user,String oldpassword){

        User olduser = this.userMapper.getOne(user.getId());
        if (StringUtils.equals(olduser.getPassword(),MD5Utils.toMD5(oldpassword+olduser.getSalt()))){

            String salt= StringUtils.substring(UUIDUtils.getUUID(),0,10);
            user.setSalt(salt);

            user.setLastLoginTime(olduser.getLastLoginTime());
            user.setLastLoginIp(olduser.getLastLoginIp());
            //盐加密码
            user.setPassword(MD5Utils.toMD5(user.getPassword()+salt));
            //用户信息加密,拿到密码 得从新生成盐
            this.userMapper.save(user);
            return "ok";
        }else{
            return null;
        }
    }

    /**
     * 返回用户信息
     */
    @Override
    @Transactional
    public  User findUser(){
        List<User> all = this.userMapper.findAll();
        if (all.size() == 0){
            //创建一个用户
            this.createDefaultUser();
        }
        return this.userMapper.getOne(1);
    }

    /**
     *
     */
    @Override
    @Transactional
    public void createDefaultUser(){
        //用户信息加密
        String salt= StringUtils.substring(UUIDUtils.getUUID(),0,10);
        User user = new User();
        user.setId(1);
        user.setPassword(MD5Utils.toMD5("admin"+salt));
        user.setUsername("admin");
        user.setSalt(salt);
        user.setLastLoginIp("127.0.0.1");
        user.setLastLoginTime(new Date());
        this.userMapper.save(user);
    }

    @Override
    public Map<String,String> userLogin(String username, String password) throws Exception {
        //返回 的map
        Map<String,String> map = new HashMap<>();
        User user = new User();
        user.setUsername(username);
        //通过用户名查询用户
        Example<User> example =  Example.of(user);
        //数据库中的密码
        User localUser = null;
        try {
            //数据库中仅有的一个用户
             localUser = this.userMapper.findOne(example).get();
        }catch (Exception e){
            e.printStackTrace();
        }
        //如果查不到用，返回 一个值为Null的Map->user
        if (localUser == null){
            map.put("user",null);
            return map;
        }
        String localPassword = localUser.getPassword();
        //加密的发过来的密码
        String md5Password = MD5Utils.toMD5(password +localUser.getSalt());

        String jwt = null;
        if (StringUtils.equals(localPassword,md5Password)){
            //加密信息
            Map<String,String> md5map = new HashMap<>();
            md5map.put("id",localUser.getId()+"");
            md5map.put("username",localUser.getUsername());
            String userThing = userThing = OBJECT_MAPPER.writeValueAsString(md5map);
            //过期时间秒为单位 ： 30分钟
            jwt = JWTUtils.createJWT(UUIDUtils.getUUID(), userThing,7200);
        }
        //返回登录信息
        map.put("user",jwt);
        return map;
    }

}
