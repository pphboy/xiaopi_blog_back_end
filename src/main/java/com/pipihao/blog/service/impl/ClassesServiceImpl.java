package com.pipihao.blog.service.impl;

import com.pipihao.blog.mapper.ClassesMapper;
import com.pipihao.blog.pojo.Classes;
import com.pipihao.blog.service.ArticleService;
import com.pipihao.blog.service.ClassesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class ClassesServiceImpl implements ClassesService {

    @Autowired
    private ClassesMapper classesMapper;

    @Autowired
    private ArticleService articleService;

    /**
     * 保存一个classes
     *
     * @param classes
     */
    @Override
    @Transactional
    public void saveClasses(Classes classes) {
        this.classesMapper.save(classes);
    }

    /**
     * 分页查询所有的分类
     *
     * @param rows 页数
     * @param size 每页条数
     * @return
     */
    @Override
    public Page<Classes> findAllClassesByPage(Integer rows, Integer size) {
        Sort sort = Sort.by(Sort.Direction.DESC, "id");
        Pageable pageable = PageRequest.of(rows, size, sort);
        return this.classesMapper.findAll(pageable);
    }

    @Override
    public Classes findClassesById(Integer id) {
        return this.classesMapper.getOne(id);
    }


    /**
     * 修改一下分类
     *
     * @param classes
     */
    @Override
    public void updateClasses(Classes classes) {
        this.classesMapper.save(classes);
    }

    /**
     * 删除一个分类
     *
     * @param id
     */
    @Override
    public void deleteClassesById(Integer id) {
        Classes classes = this.findClassesById(id);
        //删除当前文章的所有分类
        classes.getArticleSet().forEach(s -> this.articleService.deleteArticleClassesIdById(s.getId(), id));
        //删除当前分类
        classes.setArticleSet(new ArrayList<>());
        this.classesMapper.delete(classes);
    }

    /**
     * 获取全部分类
     *
     * @return
     */
    @Override
    public List<Classes> findAllClass() {
        return this.classesMapper.findAll();
    }

}
