package com.pipihao.blog.service.impl;


import com.pipihao.blog.mapper.SiteSetMapper;
import com.pipihao.blog.pojo.SiteSet;
import com.pipihao.blog.service.SiteSetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class SiteSetServiceImpl implements SiteSetService {

    @Autowired
    private SiteSetMapper siteSetMapper;

    /**
     * 创建一个默认的网站配置
     */
    @Override
    @Transactional
    public void saveDefaultSiteSet() {
        SiteSet siteSet = new SiteSet();
        siteSet.setId(1);
        siteSet.setKeywords("皮皮豪,小皮,皮皮豪博客");
        siteSet.setLeaveShow(true);
        siteSet.setSiteShow(true);
        siteSet.setWebSiteIntroduce("小皮博客是一个基于springboot+springmvc+springjpa的博客"); //本站介绍
        siteSet.setWebSiteName("皮皮豪");
        this.siteSetMapper.save(siteSet);
    }

    /**
     * 更新网站配置信息
     * @param siteSet
     */
    @Override
    @Transactional
    public void updateService(SiteSet siteSet){
        this.siteSetMapper.save(siteSet);
    }
    /**
     * 返回 SiteSet不需要任何条件
     * @return
     */
    @Override
    @Transactional
    public SiteSet findSiteSet(){
        //如果all没有数据，则size为0,
        List<SiteSet> all = this.siteSetMapper.findAll();
        //如果拿不到数据说明是新网站
        if (all.size() == 0){
            //创新一个行网站配置
            this.saveDefaultSiteSet();
        }
        return this.siteSetMapper.getOne(1);
    }
}
