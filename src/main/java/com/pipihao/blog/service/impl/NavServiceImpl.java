package com.pipihao.blog.service.impl;

import com.pipihao.blog.mapper.NavMapper;
import com.pipihao.blog.pojo.Nav;
import com.pipihao.blog.service.NavService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NavServiceImpl implements NavService {
    @Autowired
    private NavMapper navMapper;

    @Override
    public void saveNav(Nav nav) {
        this.navMapper.save(nav);
    }

    @Override
    public void deleteNav(Integer id) {
        this.navMapper.deleteById(id);
    }

    @Override
    public List<Nav> findAll(){
        return this.navMapper.findAll();
    }
}
