package com.pipihao.blog.service.impl;

import com.pipihao.blog.mapper.LabelMapper;
import com.pipihao.blog.pojo.Label;
import com.pipihao.blog.service.ArticleService;
import com.pipihao.blog.service.LabelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class LabelServiceImpl implements LabelService {

    @Autowired
    private LabelMapper labelMapper;
    @Autowired
    private ArticleService articleService;

    /**
     * 保存一个标签
     * @param  label
     */
    @Override
    @Transactional
    public void saveLabel(Label label){
        this.labelMapper.save(label);
    }

    /**
     * 分页查询所有的标签
     * @return
     */
    @Override
    public Page<Label> findAllLabelByPage(Integer rows, Integer size){
        Sort sort = Sort.by(Sort.Direction.DESC,"id");
        Pageable pageable = PageRequest.of(rows,size,sort);
        return this.labelMapper.findAll(pageable);
    }

    /**
     * 通过id查询一个标签
     * @param id
     * @return
     */
    @Override
    public Label findLabelById(Integer id){
        return this.labelMapper.getOne(id);
    }

    /**
     * 修改一下标签
     * @param label
     */
    @Override
    @Transactional
    public void updateLabel(Label label){
        this.labelMapper.save(label);
    }

    /**
     *  删除一个标签
     * @param id
     */
    @Override
    @Transactional
    public void deleteLabelById(Integer id){
        Label label= this.labelMapper.getOne(id);
        //s.getId是article的Id,后面的id是label的id
        label.getArticleList().forEach(s->this.articleService.deleteArticleLabelById(s.getId(),id));
        this.labelMapper.delete(label);
    }

    @Override
    @Transactional
    public List<Label> findAll(){
        Sort sort = Sort.by(Sort.Direction.DESC,"id");
        return this.labelMapper.findAll(sort);
    }
}
