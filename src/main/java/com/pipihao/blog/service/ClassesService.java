package com.pipihao.blog.service;

import com.pipihao.blog.pojo.Classes;
import org.springframework.data.domain.Page;

import java.util.List;

public interface ClassesService {

    /**
     * 保存一个classes
     * @param classes
     */
    public void saveClasses(Classes classes);

    /**
     * 分页查询所有的分类
     * @return
     */
    public Page<Classes> findAllClassesByPage(Integer rows, Integer size);

    /**
     * 通过id查询类
     * @param id
     * @return
     */
    public Classes findClassesById(Integer id);

    /**
     * 修改一下分类
     * @param classes
     */
    public void updateClasses(Classes classes);

    /**
     *  删除一个分类
     * @param id
     */
    public void deleteClassesById(Integer id);

    /**
     * 获取全部的类
     */
    public List<Classes> findAllClass();

}
