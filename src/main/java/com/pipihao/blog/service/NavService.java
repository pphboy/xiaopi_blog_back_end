package com.pipihao.blog.service;

import com.pipihao.blog.pojo.Nav;

import java.util.List;

public interface NavService {

    public void saveNav(Nav nav);

    public void deleteNav(Integer id);

    public List<Nav> findAll();
}
