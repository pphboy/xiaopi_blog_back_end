package com.pipihao.blog.service;

import com.pipihao.blog.pojo.LeavingMessage;
import org.springframework.data.domain.Page;

public interface LeavingMessageService {

    /**
     * 保存一个leavingMessage
     * @param leavingMessage
     */
    public void saveLeavingMessage(LeavingMessage leavingMessage);

    /**
     * 分页查询 leavingMessage
     * @param rows
     * @param size
     */
    public Page<LeavingMessage> findAllLeavingMessageByPage(Integer rows, Integer size);

    /**
     * 删除一个留言
     * @param id
     */
    public void deleteLeavingMessageById(Integer id);
}
