package com.pipihao.blog.service;


import com.pipihao.blog.pojo.SiteSet;

public interface SiteSetService {


    /**
     * 创表一个默认的网站配置
     */
    public void saveDefaultSiteSet();

    /**
     * 更新网站配置信息
     * @param siteSet
     */
    public void updateService(SiteSet siteSet);

    /**
     * 返回 SiteSet不需要任何条件
     * @return
     */
    public SiteSet findSiteSet();
}
