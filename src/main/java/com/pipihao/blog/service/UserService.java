package com.pipihao.blog.service;

import com.pipihao.blog.pojo.User;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

public interface UserService {

    /**
     * 更新用户信息
     * @param user
     * @param oldpassword
     */
    public String updateUser(User user,String oldpassword);

    /**
     * 返回用户信息
     */
    public  User findUser();

    /**
     * 创建一个默认用记户
     */
    public void createDefaultUser();

    /**
     * 登录
     */
    public Map<String,String> userLogin(String username, String password) throws Exception;
}
