package com.pipihao.blog.service;

import com.pipihao.blog.pojo.Article;
import com.pipihao.blog.pojo.Classes;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Set;

public interface ArticleService {

    /**
     * 保存一个Article
     * @param article
     * @param class_ids
     */
    public void saveArticle(Article article,List<Integer> class_ids,List<Integer> labelIds);

    /**
     * 过通id查询一篇文章
     * @param id
     * @return
     */
    public Article findArticleById(Long id);

    /**
     * 分页查询
     * @return page里面包含了总页数和总条数
     */
    public Page<Article> findArticleByPage(Integer rows, Integer size);


    /**
     * 修改 article
     * @param article
     */
    public void updateArticle(Article article);

    /**
     * 删除一个article
     * @param id
     */
    public void deleteArticleById(Long id);

    /**
     * 删除当前文章下面的分类
     * @param articledId
     * @param classId
     */
    public void deleteArticleClassesIdById(Long articledId,Integer classId);

    /**
     * 删除一本文章所包含的标签
     * @param articleId
     * @param labelId
     */
    public void deleteArticleLabelById(Long articleId,Integer labelId);

    /**
     * 查询最火热的几篇文章
     * @return
     */
    public Page<Article> findHotArticleByPage();

    /**
     * article的watchnum+1
     */
    public void addWatchNumber(Article article);

    public List<Article> findArticleByClassesId(Integer classId,Integer page,Integer size);

    public List<Article> findArticleByLabelId(Integer classId,Integer page,Integer size);

    public Page<Article> findAllByClassId(Integer classId,Integer page,Integer size);
    public Page<Article> findAllByLabelId(Integer labelId,Integer page,Integer size);


    Integer getArtileTotalNumberByLabelId(Integer labelId);
    Integer getArtileTotalNumberByClassId(Integer classId);
}
