package com.pipihao.blog.service;

import com.pipihao.blog.pojo.Label;
import org.springframework.data.domain.Page;

import java.util.List;

public interface LabelService{

    /**
     * 保存一个标签
     * @param  label
     */
    public void saveLabel(Label label);

    /**
     * 分页查询所有的标签
     * @return
     */
    public Page<Label> findAllLabelByPage(Integer rows, Integer size);

    /**
     * 通过id查询一个标签
     * @param id
     * @return
     */
    public Label findLabelById(Integer id);

    /**
     * 修改一下标签
     * @param label
     */
    public void updateLabel(Label label);

    /**
     *  删除一个标签
     * @param id
     */
    public void deleteLabelById(Integer id);

    public List<Label> findAll();
}
