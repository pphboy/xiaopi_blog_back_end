package com.pipihao.blog.controller;

import com.pipihao.blog.config.UserLoginToken;
import com.pipihao.blog.pojo.Label;
import com.pipihao.blog.pojo.LeavingMessage;
import com.pipihao.blog.service.LeavingMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("leave")
@UserLoginToken
public class LeavingMessageController {

    @Autowired
    private LeavingMessageService leavingMessageService;

    /**
     * 分页查询一个标签
     * @param page
     * @param size
     * @return
     */
    @UserLoginToken
    @GetMapping
    public ResponseEntity<Page<LeavingMessage>> findLeavingMessageListByPage(
            @RequestParam(value = "page",defaultValue = "1") Integer page,
            @RequestParam(value = "size",defaultValue = "5") Integer size
    ){
        if (page  <= 0 || size <=0){
            return ResponseEntity.badRequest().build();
        }
        page-=1;
        return ResponseEntity.ok(this.leavingMessageService.findAllLeavingMessageByPage(page,size));
    }

    /**
     * 删除一个留言
     * @param leaveId
     * @return
     */
    @UserLoginToken
    @DeleteMapping
    public ResponseEntity<Void> deleteLabelById(@RequestParam(value = "leaveId",required = true)Integer leaveId){
        if (leaveId== null || leaveId<=0){
            return ResponseEntity.badRequest().build();
        }
        this.leavingMessageService.deleteLeavingMessageById(leaveId);
        return ResponseEntity.noContent().build();
    }
}
