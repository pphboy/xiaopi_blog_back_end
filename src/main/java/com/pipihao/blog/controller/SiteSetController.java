package com.pipihao.blog.controller;

import com.pipihao.blog.config.UserLoginToken;
import com.pipihao.blog.pojo.SiteSet;
import com.pipihao.blog.service.SiteSetService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("set")
@UserLoginToken
public class SiteSetController {

    @Autowired
    private SiteSetService siteSetService;

    @UserLoginToken
    @PostMapping
    public ResponseEntity<Void> updateSiteSet(SiteSet siteSet){
        if (siteSet == null || siteSet.getId() != 1||
                StringUtils.isBlank(siteSet.getWebSiteName())||
                StringUtils.isBlank(siteSet.getKeywords())
        ){
            return ResponseEntity.badRequest().build();
        }
        this.siteSetService.updateService(siteSet);
        return ResponseEntity.noContent().build();
    }

    @GetMapping
    @UserLoginToken
    public ResponseEntity<SiteSet> findSiteSet(){
        SiteSet siteSet = this.siteSetService.findSiteSet();
        return ResponseEntity.ok(siteSet);
    }
}
