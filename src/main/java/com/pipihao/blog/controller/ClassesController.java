package com.pipihao.blog.controller;

import com.pipihao.blog.config.UserLoginToken;
import com.pipihao.blog.pojo.Classes;
import com.pipihao.blog.service.ClassesService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("class")
public class ClassesController {

    @Autowired
    private ClassesService classesService;


    /**
     * 增加一个类和更新一个类
     * @param classes
     * @return
     */
    @PostMapping
    @UserLoginToken
    public ResponseEntity<Void> saveClasses(Classes classes){
        if (classes==null ||
            StringUtils.isBlank(classes.getClassName()) ||
            StringUtils.isBlank(classes.getIntroduce())){
            return ResponseEntity.badRequest().build();
        }
        this.classesService.saveClasses(classes);
        return ResponseEntity.noContent().build();
    }

    /**
     * 分页查询类集
     * @param page
     * @param size
     * @return
     */
    @UserLoginToken
    @GetMapping
    public ResponseEntity<Page<Classes>>
    getClasses(@RequestParam(value = "page",defaultValue = "1")Integer page,
               @RequestParam(value = "size",defaultValue = "5")Integer size){
        if (page <= 0 || size<=0){
            return ResponseEntity.badRequest().build();
        }
        page-=1;
        return ResponseEntity.ok(this.classesService.findAllClassesByPage(page,size));
    }

    /**
     * 删除一个类
     * @param classId
     * @return
     */
    @UserLoginToken
    @DeleteMapping
    public ResponseEntity<Void> deleteClassesById(@RequestParam("classId")Integer classId){
        if (classId==null || classId <= 0) return ResponseEntity.badRequest().build();
        this.classesService.deleteClassesById(classId);
        return ResponseEntity.noContent().build();
    }
}
