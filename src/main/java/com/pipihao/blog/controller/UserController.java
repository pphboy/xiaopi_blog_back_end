package com.pipihao.blog.controller;

import com.pipihao.blog.config.PassToken;
import com.pipihao.blog.config.UserLoginToken;
import com.pipihao.blog.pojo.User;
import com.pipihao.blog.service.UserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

@Controller
@RequestMapping("user")
@UserLoginToken
public class UserController {

    @Autowired
    private UserService userService;

    @UserLoginToken
    @PostMapping
    public ResponseEntity<Void> updateUserInfo(User user, @RequestParam(value = "oldpassword",required = true)String oldpassword){
        if (user.getId() != 1 ||
            StringUtils.isBlank(user.getUsername())||
            StringUtils.isBlank(user.getPassword())
        ) return ResponseEntity.badRequest().build();
        if (this.userService.updateUser(user,oldpassword)==null) return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        return ResponseEntity.noContent().build();
    }

    @UserLoginToken
    @GetMapping
    public ResponseEntity<User> getUserInfo(){
        return ResponseEntity.ok(this.userService.findUser());
    }

    @PostMapping("/login")
    public ResponseEntity<Map<String,String>> login(@RequestParam(value = "username",required = true) String  username, @RequestParam(value = "password",required = true)String password) throws Exception {
        Map<String, String> stringStringMap = this.userService.userLogin(username, password);
        return ResponseEntity.ok(stringStringMap);
    }
}
