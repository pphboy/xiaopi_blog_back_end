package com.pipihao.blog.controller;

import com.pipihao.blog.config.UserLoginToken;
import com.pipihao.blog.pojo.Article;
import com.pipihao.blog.service.ArticleService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("article")
public class ArticleController {

    @Autowired
    private ArticleService articleService;

    /**
     * 保存一个文章,和更新
     * @param article
     * @param classArray
     * @param labelArray
     * @return
     */
    @UserLoginToken
    @PostMapping
    public ResponseEntity<Void> saveArticle(
            Article article,
        @RequestParam("classArray")  List<Integer> classArray,
        @RequestParam("labelArray")  List<Integer> labelArray
    ){
        if (classArray.isEmpty()||article==null){
            return ResponseEntity.badRequest().build();
        }
        articleService.saveArticle(article,classArray,labelArray);
        return ResponseEntity.noContent().build();
    }

    /**
     * 分页查询
     * @param page
     * @param size
     * @return
     */
    @GetMapping
    @UserLoginToken
    public ResponseEntity<Page<Article>> getArticle(@RequestParam(value = "page",defaultValue = "1")Integer page,@RequestParam(value = "size",defaultValue = "5")Integer size){
        if (page < 1||size<1){
            return ResponseEntity.badRequest().build();
        }
        page-=1;
        return ResponseEntity.ok(this.articleService.findArticleByPage(page,size));
    }

    /**
     * 删除一个Article
     * @param articleId
     * @return
     */
    @DeleteMapping
    @UserLoginToken
    public ResponseEntity<Void> deleteArticleById(@RequestParam(value = "articleId",required = true) Long articleId){
        if (articleId == null|| articleId<=0) return ResponseEntity.badRequest().build();
        this.articleService.deleteArticleById(articleId);
        return ResponseEntity.noContent().build();
    }

    /**
     * 通过id查询一个artile
     * @param articleId
     * @return
     */
    @GetMapping("{id}")
    @UserLoginToken
    public ResponseEntity<Article> findArticleById(@PathVariable(value = "articleId",required = true) Long articleId){
        if (articleId<=0) return ResponseEntity.badRequest().build();
        return ResponseEntity.ok(this.articleService.findArticleById(articleId));
    }

}
