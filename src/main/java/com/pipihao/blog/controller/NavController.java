package com.pipihao.blog.controller;

import com.pipihao.blog.pojo.Nav;
import com.pipihao.blog.service.NavService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("nav")
public class NavController {

    @Autowired
    private NavService navService;

    @GetMapping
    public ResponseEntity<List<Nav>> findAll(){
        return ResponseEntity.ok(this.navService.findAll());
    }

    @PostMapping
    public ResponseEntity<Void> saveNav(Nav nav){
        if (StringUtils.isBlank(nav.getNavName())|| StringUtils.isBlank(nav.getNavUrl())){
            return ResponseEntity.badRequest().build();
        }
        this.navService.saveNav(nav);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping
    public ResponseEntity<Void> deleteNav(@RequestParam(value = "navId",required = false) Integer id){
        if (id<0){
            return ResponseEntity.badRequest().build();
        }
        this.navService.deleteNav(id);
        return ResponseEntity.noContent().build();
    }
}
