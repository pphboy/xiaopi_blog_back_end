package com.pipihao.blog.controller;

import com.pipihao.blog.config.UserLoginToken;
import com.pipihao.blog.pojo.Label;
import com.pipihao.blog.service.LabelService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("label")
public class LabelController {

    @Autowired
    private LabelService labelService;

    /**
     * 保存一个标签
     * @param label
     * @return
     */
    @UserLoginToken
    @PostMapping
    public ResponseEntity<Void> saveLabel(Label label){
        if (label == null || StringUtils.isBlank(label.getLabelName())){
            return ResponseEntity.badRequest().build();
        }
        this.labelService.saveLabel(label);
        return ResponseEntity.noContent().build();
    }

    /**
     * 分页查询一个标签
     * @param page
     * @param size
     * @return
     */
    @UserLoginToken
    @GetMapping
    public ResponseEntity<Page<Label>> findLabelListByPage(
            @RequestParam(value = "page",defaultValue = "1") Integer page,
            @RequestParam(value = "size",defaultValue = "5") Integer size
    ){
        if (page  <= 0 || size <=0){
            return ResponseEntity.badRequest().build();
        }
        page-=1;
        return ResponseEntity.ok(this.labelService.findAllLabelByPage(page,size));
    }

    /**
     * 删除一个标签
     * @param labelId
     * @return
     */
    @UserLoginToken
    @DeleteMapping
    public ResponseEntity<Void> deleteLabelById(@RequestParam(value = "labelId",required = true)Integer labelId){
        if (labelId == null || labelId <=0){
            return ResponseEntity.badRequest().build();
        }
        this.labelService.deleteLabelById(labelId);
        return ResponseEntity.noContent().build();
    }

}
