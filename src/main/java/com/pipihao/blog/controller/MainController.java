package com.pipihao.blog.controller;

import com.pipihao.blog.config.PassToken;
import com.pipihao.blog.pojo.*;
import com.pipihao.blog.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.sql.Time;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;


@Controller
public class MainController {
    @Autowired
    private ArticleService articleService;

    @Autowired
    private SiteSetService siteSetService;

    @Autowired
    private LabelService labelService;
    @Autowired
    private NavService navService;

    @Autowired
    private ClassesService classesService;

    @Autowired
    private LeavingMessageService leavingMessageService;



    @GetMapping(path = {"/","index.html"})
    @PassToken
    public String requestIndex(Model model, @RequestParam(value = "page",defaultValue = "1")Integer page,@RequestParam(value = "size",defaultValue = "10")Integer size){
        page-=1;
        SiteSet siteSet = siteSetService.findSiteSet();
        Page<Article> articlePage = this.articleService.findArticleByPage(page, size);
        model.addAttribute("siteSet",siteSet);
        model.addAttribute("page",articlePage);
        return "index";
    }

    @GetMapping("sidebar.html")
    @PassToken
    public String getSidebar(Model model){
        model.addAttribute("hotarticle",articleService.findHotArticleByPage().getContent());
        model.addAttribute("classes",this.classesService.findAllClass());
        model.addAttribute("labels",this.labelService.findAll());
        model.addAttribute("articleService",this.articleService);
        model.addAttribute("leaves",this.leavingMessageService.findAllLeavingMessageByPage(0,10));
        return "sidebar";
    }
    @GetMapping("header.html")
    @PassToken
    public String getHeader(Model model){
        List<Nav> all = this.navService.findAll();
        SiteSet siteSet = siteSetService.findSiteSet();
        model.addAttribute("navs",all);
        model.addAttribute("siteSet",siteSet);
        return "header";
    }
    @GetMapping("footer.html")
    @PassToken
    public String getFooter(Model model){
        return "footer";
    }

    @GetMapping("{id}")
    @PassToken
    public String showPageById(Model model, @PathVariable(value = "id")Long id){
        Article article= this.articleService.findArticleById(id);
        SiteSet siteSet = siteSetService.findSiteSet();
        this.articleService.addWatchNumber(article);
        model.addAttribute("siteset",siteSet);
        model.addAttribute("article",article);
        return "page";
    }

    /**
     * class界面
     */
    @GetMapping("clazz")
    @PassToken
    public String showClassArticleById(Model model, @RequestParam(value = "id")Integer id,@RequestParam(value = "page",defaultValue = "1") Integer page){
        Page<Article> articleByClassesId = this.articleService.findAllByClassId(id, --page, 10);
        Classes classes= this.classesService.findClassesById(id);
        SiteSet siteSet = siteSetService.findSiteSet();
        model.addAttribute("siteSet",siteSet);
        model.addAttribute("clazz",classes);
        model.addAttribute("page",articleByClassesId);
        model.addAttribute("totalpage",articleByClassesId.getTotalPages());
        model.addAttribute("pagenumber",page);
        return "classes";
    }

    /**
     * label界面
     */
    @GetMapping("labe")
    @PassToken
    public String showLabelArticleById(Model model, @RequestParam(value = "id")Integer id,@RequestParam(value = "page",defaultValue = "1") Integer page){
        Page<Article> articleByClassesId = this.articleService.findAllByLabelId(id, --page, 10);
        Label label= this.labelService.findLabelById(id);
        SiteSet siteSet = siteSetService.findSiteSet();
        model.addAttribute("siteSet",siteSet);
        model.addAttribute("label",label);
        model.addAttribute("page",articleByClassesId);
        model.addAttribute("totalpage",articleByClassesId.getTotalPages());
        model.addAttribute("pagenumber",page);
        return "labels";
    }

    /**
     * 留言信 息接口
     */
    @PostMapping("{id}")
    @PassToken
    public String sendLeaveingMSG(Model model, LeavingMessage leavingMessage,@PathVariable("id") Long id){
        Article article= this.articleService.findArticleById(id);
        SiteSet siteSet = siteSetService.findSiteSet();
        leavingMessage.setArticle(article);
        this.leavingMessageService.saveLeavingMessage(leavingMessage);
        model.addAttribute("article",article);
        model.addAttribute("siteset",siteSet);
        return "page";
    }

}
