package com.pipihao.blog;


import com.pipihao.blog.pojo.Nav;
import com.pipihao.blog.service.NavService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest(classes = BlogApplication.class)
public class TestNavService {
    @Autowired
    private NavService navService;

    @Test
    @Transactional
    @Rollback(false)
    public void saveNav(){
        Nav nav = new Nav();
        nav.setNavName("测试");
        nav.setNavUrl("http:///12312312.com");
        this.navService.saveNav(nav);
    }
}
