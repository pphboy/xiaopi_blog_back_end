package com.pipihao.blog;

import com.pipihao.blog.pojo.User;
import com.pipihao.blog.service.UserService;
import com.pipihao.blog.util.MD5Utils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.Map;

@SpringBootTest(classes = BlogApplication.class)
public class TestUserService {
    @Autowired
    private UserService userService;

    @Test
    @Transactional
    @Rollback(false)
    public void testUpdateUser(){
        User user1 = this.userService.findUser();
        System.out.println(user1);
        User user = new User();
        user.setId(1);
        user.setPassword("hc922922");
        user.setUsername("pipihao");
        user.setLastLoginIp("127.0.0.1");
        user.setLastLoginTime(new Date());
        System.out.println(user);
        this.userService.updateUser(user,"admin");
    }

    @Test
    @Transactional
    @Rollback(false)
    public void findUser(){
        User user1 = this.userService.findUser();
        System.out.println(user1);
    }

    @Test
    @Transactional
    @Rollback(false)
    public void testLogin() throws Exception {
        Map<String, String> map= this.userService.userLogin("pipihao", "hc922922");
        System.out.println(map.get("user"));
    }
    @Test
    public void testMd5(){
        String salt = "f67c47e062";
        System.out.println(MD5Utils.toMD5("hc922922" + salt));
    }
}
