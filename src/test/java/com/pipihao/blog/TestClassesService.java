package com.pipihao.blog;

import com.pipihao.blog.mapper.ArticleMapper;
import com.pipihao.blog.pojo.Classes;
import com.pipihao.blog.service.ArticleService;
import com.pipihao.blog.service.ClassesService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest(classes = BlogApplication.class)
public class TestClassesService {

    @Autowired
    private ClassesService classesService;

    @Autowired
    private ArticleMapper articleMapper;


    @Autowired
    private ArticleService articleService;
    @Test
    @Transactional
    @Rollback(false)
    public void testSaveClass(){
        for (int i = 0; i < 5; i++) {
            Classes classes = new Classes();
            classes.setClassName("SpringBootIsBest"+i*i);
            classes.setIntroduce("php is the best"+i*i);
            this.classesService.saveClasses(classes);
        }
    }

    @Test
    @Transactional
    @Rollback(false)
    public void testFindAllClass(){
        this.classesService.findAllClassesByPage(0,5).forEach(System.out::println);
    }

    @Test
    @Transactional
    @Rollback(false)
    public void testUpdate(){
        Classes classes = new Classes();
        classes.setId(1);
        classes.setClassName("皮皮");
        classes.setIntroduce("皮就是一精神，我们永不言弃");
        this.classesService.updateClasses(classes);
    }


    @Test
    @Transactional
    @Rollback(false)
    public void testDelete(){
        this.classesService.deleteClassesById(4);
    }


    @Test
    @Transactional
    @Rollback(false)
    public void test() {
        this.articleMapper.findAllByClassId(4, PageRequest.of(1,2)).forEach(System.out::println);
    }
}
