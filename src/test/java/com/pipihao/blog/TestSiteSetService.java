package com.pipihao.blog;

import com.pipihao.blog.pojo.SiteSet;
import com.pipihao.blog.service.SiteSetService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest(classes = BlogApplication.class)
public class TestSiteSetService {

    @Autowired
    private SiteSetService siteSetService;

    @Test
    @Transactional
    @Rollback(false)
    public void updateSiteSet(){
        SiteSet siteSet1 = this.siteSetService.findSiteSet();
        System.out.println(siteSet1);
        SiteSet siteSet = new SiteSet();
        siteSet.setId(1);
        siteSet.setKeywords("123123");
        siteSet.setLeaveShow(false);
        siteSet.setSiteShow(false);
        siteSet.setWebSiteIntroduce("testtesetteste");
        siteSet.setWebSiteName("小皮");
        this.siteSetService.updateService(siteSet);
    }
}
