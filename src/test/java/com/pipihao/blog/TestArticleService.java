package com.pipihao.blog;

import com.pipihao.blog.mapper.ArticleMapper;
import com.pipihao.blog.pojo.Article;
import com.pipihao.blog.pojo.Label;
import com.pipihao.blog.service.ArticleService;
import com.pipihao.blog.service.LabelService;
import com.pipihao.blog.util.DOMUtils;
import com.pipihao.blog.util.HtmlUtil;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.annotation.ReadOnlyProperty;
import org.springframework.data.domain.Page;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.util.HtmlUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@SpringBootTest(classes = BlogApplication.class)
public class TestArticleService {

    @Autowired
    private ArticleService articleService;

    @Autowired
    private LabelService labelService;

    @Autowired
    private ArticleMapper articleMapper;

    @Test
    @Transactional
    @Rollback(false)
    public void testSaveArticle() {
        for (int i = 0; i < 10000; i++) {
            Article article = new Article();
            article.setTitle("键来" + i * i);
            article.setCreateTime(new Date());
            article.setEditTime(new Date());
            article.setWatchNum(100l + i);
            article.setUsername("皮皮豪" + i * i);
            article.setContent("测测试内容，我懒得改了测试内容，我懒得改了测试内容，我懒得改了测试内容，我懒得改了测试内容，我懒得改了测试内容，我懒得改了测试内容，我懒得改了测试内容，我懒得改了测试内容，我懒得改了测试内容，我懒得改了测试内容，我懒得改了测试内容，我懒得改了测试内容，我懒得改了测试内容，我懒得改了测试内容，我懒得改了测试内容，我懒得改了测试内容，我懒得改了测试内容，我懒得改了测试内容，我懒得改了测试内容，我懒得改了测试内容，我懒得改了测试内容，我懒得改了测试内容，我懒得改了测试内容，我懒得改了测试内容，我懒得改了测试内容，我懒得改了测试内容，我懒得改了试内容，我懒得改了" + i * i);
            article.setShowOk(true);
            List<Integer> ids = new ArrayList<>();
            ids.add(4);
            ids.add(5);
            articleService.saveArticle(article, ids,ids);
        }
    }

    @Test
    @Transactional
    public void testFindArticleByClassList(){
        List<Article> articleByClassesId = this.articleService.findArticleByClassesId(4, 1, 10);
        System.out.println(articleByClassesId.get(1).getId());
    }

    @Test
    @Transactional
    @Rollback(false)
    public void testFindArticleById() throws DocumentException {
        Article article = articleService.findArticleById(332l);
        String xmlByString = HtmlUtil.getText(article.getContent());
        System.out.println(xmlByString);
//        System.out.println(article);
    }

    @Test
    @Transactional
    @Rollback(false)
    public void testFindArticleByPage() {
        articleService.findArticleByPage(1, 5).forEach(System.out::println);
        Page<Article> articleByPage = articleService.findArticleByPage(1, 5);
        System.out.println(articleByPage.getTotalElements());
        System.out.println(articleByPage.getTotalPages());
    }

    @Test
    @Transactional
    @Rollback(false)
    public void testUpdateArticle() {
        Article article = new Article();
        article.setId(1l);
        article.setTitle("键来,天不生我键盘侠");
        article.setIndexImage("pipqwjreew rw");
        article.setCreateTime(new Date());
        article.setEditTime(new Date());
        article.setWatchNum(99999l);
        article.setUsername("皮皮豪");
        article.setContent("测试内容，我懒得改了");
        article.setShowOk(false);
        articleService.updateArticle(article);
    }
    @Test
    @Transactional
    @Rollback(false)
    public void testDeleteArticle(){
        articleService.deleteArticleById(37l);
    }

    @Test
    public void testFindAllCountByLabelId(){
        System.out.println(this.articleMapper.getArtileTotalNumberByLabelId(4));;
    }

    @Test
    public void testFindAllCountByClassId(){
        System.out.println(this.articleMapper.getArtileTotalNumberByClassId(4));;
    }
}
