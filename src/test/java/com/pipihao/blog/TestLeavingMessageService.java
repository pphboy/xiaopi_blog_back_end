package com.pipihao.blog;

import com.pipihao.blog.pojo.LeavingMessage;
import com.pipihao.blog.service.ArticleService;
import com.pipihao.blog.service.LeavingMessageService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import javax.xml.crypto.Data;
import java.util.Date;

@SpringBootTest(classes = BlogApplication.class)
public class TestLeavingMessageService {

    @Autowired
    private LeavingMessageService leavingMessageService;

    @Autowired
    private ArticleService articleService;

    @Test
    @Transactional
    @Rollback(false)
    public void testSaveLMSG(){
        for (int i = 0; i < 20; i++) {
            LeavingMessage leavingMessage = new LeavingMessage();
            leavingMessage.setArticle(this.articleService.findArticleById(1l));
            leavingMessage.setContent("随便写点"+i);
            leavingMessage.setEmail("pph@boy.com"+i);
            leavingMessage.setVisitorName("我乃皮某"+i);
            leavingMessage.setVisitorIp("127.0.0.1");
            leavingMessage.setCreateTime(new Date());
            this.leavingMessageService.saveLeavingMessage(leavingMessage);
        }
    }

    @Test
    @Transactional
    @Rollback(false)
    public void testFindAllByPage(){
        Page<LeavingMessage> allLeavingMessageByPage = this.leavingMessageService.findAllLeavingMessageByPage(0, 3);
        allLeavingMessageByPage.forEach(System.out::println);
    }
}
