package com.pipihao.blog;

import com.pipihao.blog.pojo.Classes;
import com.pipihao.blog.pojo.Label;
import com.pipihao.blog.service.ClassesService;
import com.pipihao.blog.service.LabelService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest(classes = BlogApplication.class)
public class TestLabelService {

    @Autowired
    private LabelService labelService;

    @Test
    @Transactional
    @Rollback(false)
    public void testSaveLabel(){
        for (int i = 0; i < 30; i++) {
            Label label = new Label();
            label.setLabelName("不错"+i*i*i);
            this.labelService.saveLabel(label);
        }
    }

    @Test
    @Transactional
    @Rollback(false)
    public void testFindAllLabelByPage(){
        this.labelService.findAllLabelByPage(0,10).forEach(System.out::println);
    }

    @Test
    @Transactional
    @Rollback(false)
    public void testFindLabelById(){
        System.out.println(this.labelService.findLabelById(1));
    }

    @Test
    @Transactional
    @Rollback(false)
    public void testUpdateLabel(){
        Label label = new Label();
        label.setId(1);
        label.setLabelName("可惜，这只是数据库");
        this.labelService.saveLabel(label);
    }

    @Test
    @Transactional
    @Rollback(false)
    public void testDeleteLabelById(){
        this.labelService.deleteLabelById(2);
    }

}
