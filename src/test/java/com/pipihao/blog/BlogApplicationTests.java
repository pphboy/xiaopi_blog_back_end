package com.pipihao.blog;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pipihao.blog.mapper.ArticleMapper;
import com.pipihao.blog.mapper.ClassesMapper;
import com.pipihao.blog.mapper.LabelMapper;
import com.pipihao.blog.mapper.LeavingMessageMapper;
import com.pipihao.blog.pojo.Article;
import com.pipihao.blog.pojo.Classes;
import com.pipihao.blog.pojo.Label;
import com.pipihao.blog.pojo.LeavingMessage;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashSet;
import java.util.Optional;

@SpringBootTest(classes = BlogApplication.class)
class BlogApplicationTests {

    @Autowired
    private ArticleMapper articleMapper;

    @Autowired
    private ClassesMapper classesMapper;

    @Autowired
    private LabelMapper labelMapper;

    @Autowired
    private LeavingMessageMapper leavingMessageMapper;

    @Test
    @Transactional
    @Rollback(false)
    void testInsert() throws JsonProcessingException {
        Article article = new Article();
        article.setTitle("皮皮豪");
        article.setIndexImage("`12123");
        article.setCreateTime(new Date());
        article.setEditTime(new Date());
        article.setWatchNum(100l);
        article.setUsername("皮皮豪");
        article.setContent("测试");
        article.setShowOk(true);
        Optional<Classes> optional= classesMapper.findById(2);
        if (optional!= null &&optional.isPresent()){
            article.getClassesList().add(optional.get());
        }
        articleMapper.save(article);
    }

    @Test
    @Transactional
    @Rollback(false)
    public void testChangeArticleClass(){
        Article one = articleMapper.getOne(1l);
        one.getClassesList().add(classesMapper.getOne(2));
        articleMapper.save(one);
    }

    @Test
    @Transactional
    @Rollback(false)
    public void testSaveClass(){
        Classes classes = new Classes();
        classes.setIntroduce("测试");
        classes.setClassName("tests");
        classesMapper.save(classes);
    }


    @Test
    public void testFindOne(){
//        Optional<Article> one = articleMapper.findById(1l);
//        Article article1 = one.get();
        System.out.println(articleMapper.getOne(1l));
    }

    @Test
    public void testFindClasses(){
        classesMapper.getOne(1).getArticleSet().forEach(System.out::println);
    }



    @Test
    @Transactional
    @Rollback(false)
    void testSaveLabel(){
        Label label = new Label();
        label.setLabelName("好看吗");
        label.getArticleList().add(articleMapper.getOne(1l));
        labelMapper.save(label);
    }

    @Test
    @Transactional
    public void testFindOneLabel(){
        Label one = labelMapper.getOne(1);
        one.getArticleList().forEach(System.out::println);
    }

    @Test
    @Transactional
    @Rollback(false)
    public void testSaveLeavingMessage(){
        Article one = articleMapper.getOne(1l);
        LeavingMessage leavingMessage = new LeavingMessage();
        leavingMessage.setVisitorName("皮皮豪");
        leavingMessage.setEmail("pph@qq.com");
        leavingMessage.setContent("没什么好说的");
        leavingMessage.setArticle(one);
        leavingMessageMapper.save(leavingMessage);
    }

}
